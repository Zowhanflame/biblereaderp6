package bibleReader;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import bibleReader.model.BibleReaderModel;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 * @author Trevor Palmatier. Implemented 2/19/2020.
 */
public class ResultView extends JPanel {

	private Dimension dimension;
	private String[] versions;
	private JScrollPane scrollPane;
	private JEditorPane editorPane;
	private JTextArea statsTextArea;
	private BibleReaderModel model;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to model to
	 * look things up.
	 * 
	 * @param myModel The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		model = myModel;
		versions = model.getVersions();
		dimension = new Dimension(752, 485);

		statsTextArea = new JTextArea(1, 75);
		statsTextArea.setEditable(false);
		editorPane = new JEditorPane("text/html", "");
		scrollPane = new JScrollPane(editorPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(dimension);
		add(statsTextArea, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);

	}

	/**
	 * Finds all verses that contain the user input, tells the user how many verses
	 * that contain the input. Only moves to displaying the verses if there are
	 * verses to display.
	 * 
	 * @param input The input that the user wants to search for.
	 */
	public void doSearch(String input) {
		ArrayList<Verse> verses = new ArrayList<Verse>();
		String searchInput = input;
		ArrayList<Reference> references = model.getReferencesContaining(searchInput);
		for (String version : versions) {
			if (version != null) {
				for (Verse verse : model.getVerses(version, references)) {
					if (verse != null)
						verses.add(verse);
				}
			}
		}
		statsTextArea.setText("There are " + verses.size() + " verses containing the word(s): " + searchInput);
		if (verses.size() > 0) {
			displaySearchResults(verses);
		}
	}

	/**
	 * Creates and shows the display for the given list of verses.
	 * 
	 * @param verses The list of verses to be displayed.
	 */
	private void displaySearchResults(ArrayList<Verse> verses) {
		Collections.sort(verses);
		// A place to build the html document that the JEditorPane will display.
		StringBuilder result = new StringBuilder();
		// A non existent verse so that the reference matches the verse text in the
		// right version columns.
		Verse lastVerse = new Verse(BookOfBible.Dummy, 1, 1, "");
		result.append("<table><tr><th>Verse</th>");
		// Create a heading for each bible version in the model.
		for (String version : versions) {
			result.append("<th>" + version + "</th>");
		}
		// Display the verse under the appropriate heading.
		for (Verse verse : verses) {
			if (!verse.getReference().equals(lastVerse.getReference())) {
				result.append("</tr><tr><td>" + verse.getReference().toString() + "</td>");
			}
			result.append("<td>" + verse.getText() + "</td>");
			lastVerse = verse;
		}
		result.append("</tr></table>");
		editorPane.setText(result.toString());
		editorPane.setCaretPosition(0);
	}

}
