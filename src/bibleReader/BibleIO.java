package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @author Trevor Palmatier. Modified February 13, 2020.
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(bibleFile));

			// sets the defaults for the title and description.
			String abrv = "unknown";
			String desc = "";
			String currentLine;

			// sets the title and description if there is data in the file for it.
			String[] bibleTitleAndDesc = reader.readLine().split(": ");
			if (bibleTitleAndDesc.length == 2) {
				abrv = bibleTitleAndDesc[0];
				desc = bibleTitleAndDesc[1];
			} else if (!bibleTitleAndDesc[0].isEmpty()) {
				abrv = bibleTitleAndDesc[0];
			}
			// creates the list of verses in the bible and reads in the verses from the
			// file.
			VerseList someTitle = new VerseList(abrv, desc);

			currentLine = reader.readLine();
			while (currentLine != null) {
				// splits the book from the chapter and verse from the text.
				String[] parts = currentLine.split("@");
				// verifies that the data was read in as expected.
				if (parts.length <= 1) {
					reader.close();
					return null;
				}
				// verifies the book abbreviation is valid.
				BookOfBible book = BookOfBible.getBookOfBible(parts[0]);
				if (book == null) {
					reader.close();
					return null;
				}
				// splits chapter and verse and verifies that it was successful.
				String[] chapAndVerse = parts[1].split(":");
				if (chapAndVerse.length <= 1) {
					reader.close();
					return null;
				}
				int chap = Integer.parseInt(chapAndVerse[0]);
				int verse = Integer.parseInt(chapAndVerse[1]);

				someTitle.add(new Verse(book, chap, verse, parts[2]));
				currentLine = reader.readLine();
			}
			reader.close();
			return someTitle;
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		} catch (NumberFormatException e2) {
			e2.printStackTrace();
			return null;
		}

	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		// TODO Implement me: Stage 8

		// The XMV is sort of XML-like, but it doesn't have end tags.
		// No description of the file format is given here.
		// You need to look at the file to determine how it should be parsed.

		// TODO Documentation: Stage 8 (Update the Javadoc comment to describe
		// the format of the file.)
		return null;
	}

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		// TODO Implement me: Stage 8
		// Don't forget to write the first line of the file.
		// HINT: This and the next method are very similar. It seems like you
		// might be
		// able to implement one of them and then call it from the other one.
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		// TODO Implement me: Stage 8
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		// TODO Implement me: Stage 8
		// This one should be really simple.
		// My version is 4 lines of code (not counting the try/catch code).
	}
}
