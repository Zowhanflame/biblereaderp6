package bibleReader.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 */
public class BibleReaderModel implements MultiBibleModel {

	private ArrayList<Bible> bibles;
	private ArrayList<String> versions;
	private boolean versionsIsSorted;

	/**
	 * Default constructor. Instantiates the key fields.
	 */
	public BibleReaderModel() {
		bibles = new ArrayList<Bible>();
		versions = new ArrayList<String>(1);
		versionsIsSorted = true;
	}

	@Override
	public String[] getVersions() {
		if (!versionsIsSorted) {
			Collections.sort(versions);
			versionsIsSorted = true;
		}
		return versions.toArray(new String[1]);
	}

	@Override
	public int getNumberOfVersions() {
		return bibles.size();
	}

	@Override
	public void addBible(Bible bible) {
		if (bible != null) {
			versionsIsSorted = false;
			bibles.add(bible);
			versions.add(bible.getVersion());
		}
	}

	@Override
	public Bible getBible(String version) {
		for (Bible bible : bibles) {
			if (bible.getVersion().equals(version))
				return bible;
		}
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		ArrayList<Reference> refsContaining = bibles.get(0).getReferencesContaining(words);
		for (int i = 1; i < bibles.size(); i++) {
			ArrayList<Reference> newReferences = bibles.get(i).getReferencesContaining(words);
			newReferences.removeAll(refsContaining);
			refsContaining.addAll(newReferences);
		}
		Collections.sort(refsContaining);
		return refsContaining;
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		Bible bible = getBible(version);
		if (bible != null) {
			return bible.getVerses(references);
		}
		return null;
	}
	// ---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of delegate
	// methods in that they call a method on the Bible class to do most of the work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}
