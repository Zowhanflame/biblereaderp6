package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Trevor Palmatier (provided the implementation). Implemented February
 *         13, 2020.
 */
public class ArrayListBible implements Bible {

	private VerseList verses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = new VerseList(verses.getVersion(), verses.getDescription(), verses.copyVerses());
	}

	@Override
	public int getNumberOfVerses() {
		return verses.size();
	}

	@Override
	public String getVersion() {
		return verses.getVersion();
	}

	@Override
	public String getTitle() {
		return verses.getDescription();
	}

	@Override
	public boolean isValid(Reference ref) {
		for (Verse compare : verses) {
			if (compare.getReference().equals(ref))
				return true;
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		Verse verse = getVerse(r);
		if (verse != null)
			return verse.getText();
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse compare : verses) {
			if (compare.getReference().equals(r))
				return compare;
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		return getVerse(new Reference(book, chapter, verse));
	}

	@Override
	public VerseList getAllVerses() {
		return new VerseList(verses.getVersion(), verses.getDescription(), verses.copyVerses());
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList versesContaining = new VerseList(verses.getVersion(), phrase);
		if (phrase == null)
			return versesContaining;
		if (phrase.equals(""))
			return versesContaining;
		String phraseLower = phrase.toLowerCase();
		for (Verse contains : verses) {
			if (contains.getText().toLowerCase().indexOf(phraseLower) != -1) {
				versesContaining.add(contains);
			}
		}
		return versesContaining;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> refsContaining = new ArrayList<Reference>();
		if (phrase == null)
			return refsContaining;
		if (phrase.equals(""))
			return refsContaining;
		String phraseLower = phrase.toLowerCase();
		for (Verse contains : verses) {
			if (contains.getText().toLowerCase().indexOf(phraseLower) != -1) {
				refsContaining.add(contains.getReference());
			}
		}
		return refsContaining;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList versesContainingRef = new VerseList(verses.getVersion(), "Arbitrary list of Verses");
		for (Reference ref : references) {
			versesContainingRef.add(getVerse(ref));
		}
		return versesContainingRef;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
