package bibleReader.tests;

import static org.junit.Assert.assertArrayEquals;
// If you organize imports, the following import might be removed and you will
// not be able to find certain methods. If you can't find something, copy the
// commented import statement below, paste a copy, and remove the comments.
// Keep this commented one in case you organize imports multiple times.
//
// import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import bibleReader.BibleIO;
import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * Tests for the Search capabilities of the BibleReaderModel class. These tests
 * assume BibleIO is working an can read in the kjv.atv file.
 * 
 * @author Chuck Cusack, January, 2013
 */
public class TestGetReferencesForPassage {
	@Rule
	public Timeout globalTimeout = new Timeout(1000);

	private static VerseList versesFromFile;
	private BibleReaderModel model;

	@BeforeClass
	public static void readFile() {
		// Our tests will be based on the KJV version for now.
		File file = new File("kjv.atv");
		// We read the file here so it isn't done before every test.
		versesFromFile = BibleIO.readBible(file);
	}

	@Before
	public void setUp() throws Exception {
		// Make a shallow copy of the verses.
		ArrayList<Verse> copyOfList = versesFromFile.copyVerses();
		// Now make a copy of the VerseList
		VerseList copyOfVerseList = new VerseList(versesFromFile.getVersion(), versesFromFile.getDescription(),
				copyOfList);

		Bible testBible = new ArrayListBible(copyOfVerseList);
		model = new BibleReaderModel();
		model.addBible(testBible);
	}

	public VerseList getVerseForReference(String reference) {
		ArrayList<Reference> list = model.getReferencesForPassage(reference);
		VerseList results = model.getVerses("KJV", list);
		return results;
	}

	@Test
	public void testSingleVerse() {
		VerseList actualVerses = getVerseForReference("John 3 : 16");
		assertEquals(1, actualVerses.size());
		assertEquals(versesFromFile.get(26136), actualVerses.get(0));

		actualVerses = getVerseForReference("Gen 1:1");
		assertEquals(1, actualVerses.size());
		assertEquals(versesFromFile.get(0), actualVerses.get(0));

		actualVerses = getVerseForReference("Revelation 22:21");
		assertEquals(1, actualVerses.size());
		assertEquals(versesFromFile.get(31101), actualVerses.get(0));

	}

	@Test
	public void testVersesFromSingleChapter() {
		VerseList actual = getVerseForReference("Ecclesiastes 3 : 1 - 8");
		List<Verse> expected = versesFromFile.subList(17360, 17368);
		assertEquals(expected.size(), actual.size());
		assertArrayEquals(expected.toArray(), actual.toArray());

		actual = getVerseForReference("Joshua 24:28-33");
		expected = versesFromFile.subList(6504, 6510);
		assertEquals(expected.size(), actual.size());
		assertArrayEquals(expected.toArray(), actual.toArray());

		actual = getVerseForReference("Psalm 23:1-6");
		expected = versesFromFile.subList(14236, 14242);
		assertEquals(expected.size(), actual.size());
		assertArrayEquals(expected.toArray(), actual.toArray());

	}

}
